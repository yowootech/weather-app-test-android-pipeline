package com.obvyah.weatherapp.utils;

import com.obvyah.weatherapp.R;

import java.util.HashMap;

public class WeatherIconUtil {

    public static final HashMap<String, Integer> ICONS;
    static {
        ICONS = new HashMap<>();
        ICONS.put("clear-day", R.drawable.ic_clear_day);
        ICONS.put("clear-night", R.drawable.ic_clear_night);
        ICONS.put("rain", R.drawable.ic_rain);
        ICONS.put("snow", R.drawable.ic_snow);
        ICONS.put("sleet", R.drawable.ic_sleet);
        ICONS.put("wind", R.drawable.ic_wind);
        ICONS.put("fog", R.drawable.ic_fog);
        ICONS.put("cloudy", R.drawable.ic_cloudy);
        ICONS.put("partly-cloudy-day", R.drawable.ic_partly_cloudy_day);
        ICONS.put("partly-cloudy-night", R.drawable.ic_partly_cloudy_night);
        ICONS.put("thunderstorm", R.drawable.ic_thunderstorm);
    }
}
