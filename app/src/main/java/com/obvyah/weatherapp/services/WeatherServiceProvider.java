package com.obvyah.weatherapp.services;

import android.util.Log;

import com.obvyah.weatherapp.events.ErrorEvent;
import com.obvyah.weatherapp.events.WeatherEvent;

import org.greenrobot.eventbus.EventBus;

import model.Currently;
import model.Weather;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherServiceProvider {

    private static final String TAG = WeatherServiceProvider.class.getSimpleName();
    private static final String BASE_URL = "https://api.darksky.net/forecast/d9bd8a35337f0b39aaaffcf7383c5da0/";
    private Retrofit retrofit;

    private Retrofit getRetrofit(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public void getWeather(double lat, double lng){
        WeatherService weatherService = getRetrofit().create(WeatherService.class);
        Call<Weather> weatherData = weatherService.getWeather(lat, lng);
        weatherData.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (response==null){
                    Log.e(TAG, "no weather data available");
                    EventBus.getDefault().post(new ErrorEvent("no weather data available"));
                    return;
                }

                Weather weather = response.body();
                Currently currently = weather.getCurrently();
                Log.e(TAG, "Temperature = " + currently.getTemperature());
                EventBus.getDefault().post(new WeatherEvent(weather));
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                EventBus.getDefault().post(new ErrorEvent("Unable to connect to weather api"));
            }
        });
    }
}
