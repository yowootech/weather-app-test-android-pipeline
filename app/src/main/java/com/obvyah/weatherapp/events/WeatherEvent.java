package com.obvyah.weatherapp.events;

import model.Weather;

public class WeatherEvent {
    private Weather weather;

    public WeatherEvent(Weather weather){
        this.weather = weather;
    }

    public Weather getWeather() {
        return weather;
    }

}
