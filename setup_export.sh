#!/usr/bin/env bash
LATEST_APK=$(ls -lrt ./app/build/outputs/apk/*.apk | tail -1 | awk -F" " '{ print $9 }') #Pick the latest build apk. 
echo $LATEST_APK
FILE_NAME=$(basename $LATEST_APK .apk)".apk" 
echo $FILE_NAME
BUILD_DATE=`date +%Y-%m-%d` #optional -- For changelog title. 
echo $BUILD_DATE
FILE_TITLE=$(basename $LATEST_APK .apk) #optional -- For changelog title.
echo $FILE_TITLE